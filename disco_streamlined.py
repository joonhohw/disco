#!/usr/bin/env python3

from scapy.all import *
import ipaddress
import threading
from queue import Queue
from datetime import datetime
import socket
import sys
import os
import subprocess
import logging
import argparse
import csv

# Check if root
if os.geteuid() != 0:
	sys.exit("This script must be run as root.")

# Parse Arguments
parser = argparse.ArgumentParser(prog = './disco_streamlined.py',
								 description = 'Scan subnets for live hosts using ICMP, SYN, and ACK scans and outputs results as a CSV file.')
parser.add_argument("-V",
					"--version",
					help = "Print version",
					action = "version",
					version= "%(prog)s 0.2")

parser.add_argument("-c",
					"--threads",
					help = "Set number of threads to be used. Defaults to 10",
					default = 10,
					type = int)

parser.add_argument("-t",
					"--timeout",
					help = "Set timeout threshold in seconds. Defaults to 1",
					default = 1,
					type = int)

parser.add_argument("-f",
					"--file",
					help = "Input as a file",
					action = "store_true")

parser.add_argument("Input",
					help = "A single CIDR subnet or a file with multiple CIDR subnets on separate newlines when used with the -f/--file flag")

parser.add_argument("Outfile",
					help = "Output filename")
					 
args = parser.parse_args()

# Print lock for multithreading
print_lock = threading.Lock()

# Logging
#logging.getLogger("scapy.runtime").setLevel(logging.ERROR)

# Progress Timer
class RepeatedTimer(object):
	def __init__(self, interval, function, *args, **kwargs):
		self._timer = None
		self.interval = interval
		self.function = function
		self.args = args
		self.kwargs = kwargs
		self.is_running = False
		self.start()
	
	def _run(self):
		self.is_running = False
		self.start()
		self.function(*self.args, **self.kwargs)
	
	def start(self):
		if not self.is_running:
			self._timer = threading.Timer(self.interval, self._run)
			self._timer.start()
			self.is_running = True
	def stop(self):
		self._timer.cancel()
		self.is_running = False

# Defined Functions
## ICMP Scan
def icmp_scan(host):
	### Craft ICMP packet
	icmp = IP(dst = host)/ICMP(id = RandShort())
	
	### Send ICMP packet and store response
	resp = sr1(icmp, timeout = args.timeout, verbose = 0)
	
	### Determine host status
	if resp is None:
		return 0
	elif resp.getlayer(ICMP).type == 0:
		print(host, "ICMP Up")
		return 1
	else:
		print("debug0")
		
## SYN Scan
def syn_scan(host):
	src_port = RandShort().__int__()
	
	### Craft SYN and RST packets
	syn = IP(dst = host)/TCP(sport = src_port, dport = 80, flags = 'S')
	rst = IP(dst = host)/TCP(sport = src_port, dport = 80, flags = 'R')
	
	### Send SYN packet and store response
	resp = sr1(syn, timeout = args.timeout, verbose = 0)
	
	### Determine host status
	if resp is None:
		return 0
	elif resp.haslayer(TCP):
		if resp.getlayer(TCP).flags == 0x12 or resp.getlayer(TCP).flags == 0x14:
			#### Send RST packet to close connection.
			sr(rst, timeout = args.timeout, verbose = 0)
			print(host, "SYN Up")
			return 1
		else:
			print("debug1")
	elif resp.haslayer(ICMP):
		if int(resp.getlayer(ICMP).type) == 3 and int(resp.getlayer(ICMP).code) in [1, 2, 3, 9, 10, 13]:
			print("debug2")
		else:
			print("debug3")
	else:
		print("debug4")
		
## ACK Scan
def ack_scan(host):
	src_port = RandShort().__int__()
	
	### Craft ACK packet
	ack = IP(dst = host)/TCP(sport = src_port, dport = 80, flags = 'A')

	### Send ACK packet and store response
	resp = sr1(ack, timeout = args.timeout, verbose = 0)
	
	### Determine host status
	if resp is None:
		return 0
	elif resp.haslayer(TCP):
		if resp.getlayer(TCP).flags == 0x4:
			print(host, "ACK Up")
			return 1
		else:
			print("debug5")
	elif resp.haslayer(ICMP):
		if int(resp.getlayer(ICMP).type) == 3 and int(resp.getlayer(ICMP).code) in [1, 2, 3, 9, 10, 13]:
			return 0
		else:
			print("debug6")
	else:
		print("debug7")
		
## Threader
def threader():
	while True:
		### Get worker from queue
		worker = q.get()
		
		### Work
		host_results = [worker]
		
		up = icmp_scan(worker)
		if not up:
			up = syn_scan(worker)
		if not up:
			up = ack_scan(worker)
			
		if up:
			with print_lock:
				results.append(host_results)
		
		### Progress counter
		global scanned_count
		scanned_count += 1
		
		### Complete job
		q.task_done()

## Print progress
def print_progress():
	print(str(scanned_count), "IP Addresses scanned...")

# Main
target_list = []
ip_count = 0
global scanned_count
scanned_count = 0
results = [["IP Address"]]

## Parse Target Subnets
if args.file:
	with open(args.Input) as f:
		ln = 1
		for line in f:
			candidate = line.strip()
			try:
				test = ipaddress.IPv4Network(candidate)
				ip_count += len(list(test))
			except ValueError:
				errmsg = "Line " + str(ln) + " is not a valid CIDR subnet"
				sys.exit(errmsg)
			target_list.append(candidate)
			ln += 1
else:
	try:
		test = ipaddress.IPv4Network(args.Input)
		ip_count += len(list(test))
	except ValueError:
		sys.exit("Input is not a valid CIDR subnet")
	target_list.append(args.Input)

## Create queue
q = Queue()

## Threading
for i in range(args.threads):
	t = threading.Thread(target=threader)
	
	t.daemon = True
	
	t.start()
	
print("Scanning", str(ip_count), "IP Addresses...")

## Add targets to queue
rt = RepeatedTimer(10, print_progress) 
start = datetime.now()
for subnet in target_list:
	for worker in ipaddress.IPv4Network(subnet):
		q.put(str(worker))

q.join()
rt.stop()
print("Scan finished! Writing results...")

## Write results
with open(args.Outfile, "w", newline = "") as f:
	writer = csv.writer(f)
	writer.writerows(results)

## Calculate time delta
end = datetime.now()
time_delta = end - start
days = divmod(time_delta.total_seconds(), 86400)
hours = divmod(days[1], 3600)
minutes = divmod(hours[1], 60)
seconds = divmod(minutes[1], 1)

print("Entire job took %d day(s), %d hour(s), %d minute(s), and %d second(s)" % (days[0], hours[0], minutes[0], seconds[0]))
