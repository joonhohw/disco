# Disco

Disco is a Python3 program built to scan CIDR subnets for live hosts using Scapy.

`disco.py` uses ICMP echo requests, SYN requests, and ACK requests to determine whether a host is up, and will output live hosts to a CSV file, indicating which scans the host responded to. You are also able to activate specific scans if required.

`disco_streamlined.py` differs from `disco.py` in that it will first scan a host with an ICMP echo request, and will only follow up with a SYN request if the ICMP echo request failed. It will then follow up with an ACK request if the SYN request failed. `disco_streamlined.py` will only write IP addresses to the output CSV file.

**Both of these scripts must be run as root.**

This program requires Scapy, which can be installed with the following command on Linux machines:
```bash
pip3 install scapy
```

## Usage

Use the command `sudo ./disco.py -h` for the help dialog.
