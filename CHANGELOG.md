# Changelog

## v0.2

- Added ACK scans.
- Added Progress tracker.
- Fixed bug where ICMP scans would fail as they would be created with packet ID "0x0".

## v0.1

- Initial commit.